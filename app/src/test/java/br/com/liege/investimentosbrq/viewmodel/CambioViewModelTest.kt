package br.com.liege.investimentosbrq.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import br.com.liege.investimentosbrq.coroutines.AppContextProvider
import br.com.liege.investimentosbrq.coroutines.TestContextProvider
import br.com.liege.investimentosbrq.model.*
import br.com.liege.investimentosbrq.repository.CambioRepositorio
import io.mockk.MockKAnnotations
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import junit.framework.Assert.assertEquals
import org.junit.*
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.math.BigDecimal
import java.math.BigInteger

@RunWith(JUnit4::class)
class CambioViewModelTes {

    @get:Rule
    val instantTaskExecutor = InstantTaskExecutorRule()

    @MockK
    lateinit var cambioRepository: CambioRepositorio

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        AppContextProvider.coroutinesContextProviderDelegate = TestContextProvider()
    }

    @Test
    fun quandoSaldoMoedaSulficiente_AtivaBotaoVender() {
        val viewModel = FragmentCambioViewModel(cambioRepository)
        val quantidadeDigitada = "10"
        val moeda = Moeda(
            name = "Dollar",
            buy = BigDecimal("5"),
            sell = BigDecimal("5"),
            variation = BigDecimal("0.5"),
            abreviacao = "USD"
        )
        val userEntity = UserEntity(moedaEmCaixaDolar = BigInteger("20"))

        //Act
        viewModel.validaBotaoVender(quantidadeDigitada, moeda, userEntity)
        //Assert
        val esperada = ValidaBotaoVenderEvent.Ativado
        val naVerdade = viewModel.viewValidaBotaoVenderEvent.value
        assertEquals(esperada, naVerdade)
    }

    @Test
    fun quandoSaldoMoedaInsuficiente_DesativaBotaoVender() {
        val viewModel = FragmentCambioViewModel(cambioRepository)
        val quantidadeDigitada = "10"
        val moeda = Moeda(
            name = "Dollar",
            buy = BigDecimal("5"),
            sell = BigDecimal("5"),
            variation = BigDecimal("0.5"),
            abreviacao = "USD"
        )
        val userEntity = UserEntity(moedaEmCaixaDolar = BigInteger("5"))

        //Act
        viewModel.validaBotaoVender(quantidadeDigitada, moeda, userEntity)
        //Assert
        val esperada = ValidaBotaoVenderEvent.Desativado
        val naVerdade = viewModel.viewValidaBotaoVenderEvent.value
        assertEquals(esperada, naVerdade)
    }

    @Test
    fun quandoDigitadoZeroOuNulo_DesativaBotaoVender() {
        val viewModel = FragmentCambioViewModel(cambioRepository)
        val quantidadeDigitada = "0" + ""
        val moeda = Moeda(
            name = "Dollar",
            buy = BigDecimal("5"),
            sell = BigDecimal("5"),
            variation = BigDecimal("0.5"),
            abreviacao = "USD"
        )
        val userEntity = UserEntity(moedaEmCaixaDolar = BigInteger("20"))

        //Act
        viewModel.validaBotaoVender(quantidadeDigitada, moeda, userEntity)
        //Assert
        val esperada = ValidaBotaoVenderEvent.Desativado
        val naVerdade = viewModel.viewValidaBotaoVenderEvent.value
        assertEquals(esperada, naVerdade)
    }

    @Test
    fun quandoSaldoSuficiente_ValidaBotaoComprar() {
        //Arrange
        val viewModel = FragmentCambioViewModel(cambioRepository)
        val quantidadeDigitada = "10"
        val moeda = Moeda(
            name = "Dollar",
            buy = BigDecimal("5"),
            sell = BigDecimal("5"),
            variation = BigDecimal("0.5"),
            abreviacao = "USD"
        )
        val userEntity = UserEntity(saldodisponivel = 50.0)

        //Act
        viewModel.validaBotaoComprar(quantidadeDigitada, moeda, userEntity)
        //Assert
        val esperada = ValidaBotaoComprarEvent.Ativado
        val naVerdade = viewModel.viewValidaBotaoComprarEvent.value
        assertEquals(esperada, naVerdade)

        //   val soma = 1+1
        //   val esperada = 2
        //    val naVerdade= soma
        //TDD
        //TEST DRIVEN DEVELOPMENT
    }

    @Test
    fun quandoSaldoInsuficiente_DesativaBotaoComprar() {
        val viewModel = FragmentCambioViewModel(cambioRepository)
        val quantidadeDigitada = "10"
        val moeda = Moeda(
            name = "Dollar",
            buy = BigDecimal("5"),
            sell = BigDecimal("5"),
            variation = BigDecimal("0.5"),
            abreviacao = "USD"
        )
        val userEntity = UserEntity(saldodisponivel = 0.0)

        //Act
        viewModel.validaBotaoComprar(quantidadeDigitada, moeda, userEntity)
        //Assert
        val esperada = ValidaBotaoComprarEvent.Desativado
        val naVerdade = viewModel.viewValidaBotaoComprarEvent.value
        assertEquals(esperada, naVerdade)
    }

    @Test
    fun quandoDigitadoZeroOuNulo_DesativaBotaoComprar() {
        val viewModel = FragmentCambioViewModel(cambioRepository)
        val quantidadeDigitada = "" + "0"
        val moeda = Moeda(
            name = "Dollar",
            buy = BigDecimal("5"),
            sell = BigDecimal("5"),
            variation = BigDecimal("0.5"),
            abreviacao = "USD"
        )
        val userEntity = UserEntity(saldodisponivel = 0.0)

        //Act
        viewModel.validaBotaoComprar(quantidadeDigitada, moeda, userEntity)
        //Assert
        val esperada = ValidaBotaoComprarEvent.Desativado
        val naVerdade = viewModel.viewValidaBotaoComprarEvent.value
        assertEquals(esperada, naVerdade)
    }

    @Test
    fun quandoCompraValida_atualizaSaldoAposCompra() {
        //Arrange
        val viewModel = FragmentCambioViewModel(cambioRepository)
        val quantidadeDigitada: BigInteger = BigInteger("5")
        val moeda = Moeda(
            name = "Dollar",
            buy = BigDecimal("5"),
            sell = BigDecimal("5"),
            variation = BigDecimal("0.5"),
            abreviacao = "USD"
        )

        val valorDaCompra = quantidadeDigitada.multiply(moeda.buy.toBigInteger())

        val userEntity = UserEntity(saldodisponivel = 50.0)
        //Act
        viewModel.atualizaSaldoAposCompra(quantidadeDigitada, moeda, userEntity)
        //Assert
        val esperada = userEntity.saldodisponivel.toDouble().minus(valorDaCompra.toDouble())
        viewModel.atualizaSaldoAposCompra(quantidadeDigitada, moeda, userEntity)

        assertEquals(esperada, userEntity.saldodisponivel)
    }

    @Test
    fun quandoCompraValida_atualizaSaldoMoedaAposCompra() {
        val viewModel = FragmentCambioViewModel(cambioRepository)
        val quantidadeDigitada: BigInteger = BigInteger("5")
        val moeda = Moeda(
            name = "Dollar",
            buy = BigDecimal("5"),
            sell = BigDecimal("5"),
            variation = BigDecimal("0.5"),
            abreviacao = "USD"
        )

        val userEntity = UserEntity(moedaEmCaixaDolar = BigInteger("5"))
        //Act
        viewModel.atualizaMoedaEmCaixaAposCompra(quantidadeDigitada, moeda, userEntity)
        //Assert
        val esperada = userEntity.moedaEmCaixaDolar.plus(quantidadeDigitada)
        viewModel.atualizaMoedaEmCaixaAposCompra(quantidadeDigitada, moeda, userEntity)

        assertEquals(esperada, userEntity.moedaEmCaixaDolar)
    }

    @Test
    fun quandoVendaValida_atualizaSaldoAposVenda() {

        val viewModel = FragmentCambioViewModel(cambioRepository)

        val quantidadeDigitada: BigInteger = BigInteger("5")
        val moeda = Moeda(
            name = "Dollar",
            buy = BigDecimal("5"),
            sell = BigDecimal("5"),
            variation = BigDecimal("0.5"),
            abreviacao = "USD"
        )

        val valorDaVenda = quantidadeDigitada.multiply(moeda.sell.toBigInteger())

        val userEntity = UserEntity(saldodisponivel = 50.0)
        //Act
        viewModel.atualizaSaldoAposVenda(quantidadeDigitada, moeda, userEntity)
        //Assert
        val esperada = userEntity.saldodisponivel.toDouble().plus(valorDaVenda.toDouble())
        viewModel.atualizaSaldoAposVenda(quantidadeDigitada, moeda, userEntity)

        assertEquals(esperada, userEntity.saldodisponivel)
    }

    @Test
    fun quandoVendaValida_atualizaSaldoMoedaAposVenda() {
        val viewModel = FragmentCambioViewModel(cambioRepository)
        val quantidadeDigitada: BigInteger = BigInteger("5")
        val moeda = Moeda(
            name = "Dollar",
            buy = BigDecimal("5"),
            sell = BigDecimal("5"),
            variation = BigDecimal("0.5"),
            abreviacao = "USD"
        )

        val userEntity = UserEntity(moedaEmCaixaDolar = BigInteger("5"))
        //Act
        viewModel.atualizaMoedaAposVenda(quantidadeDigitada, moeda, userEntity)
        //Assert
        val esperada = userEntity.moedaEmCaixaDolar.minus(quantidadeDigitada)
        viewModel.atualizaMoedaAposVenda(quantidadeDigitada, moeda, userEntity)

        assertEquals(esperada, userEntity.moedaEmCaixaDolar)
    }

    @Test
    fun quandoCompraValida_RealizaCompra() {

        val viewModel = FragmentCambioViewModel(cambioRepository)
        val quantidadeDigitada: BigInteger = BigInteger("5")
        val moeda = Moeda(
            name = "Dollar",
            buy = BigDecimal("5"),
            sell = BigDecimal("5"),
            variation = BigDecimal("0.5"),
            abreviacao = "USD"
        )
        val totalDaCompra = quantidadeDigitada.multiply(moeda.buy.toBigInteger())

        val mensagem =
            "Parabéns!\n" + "Você acabou de \n comprar" + " $quantidadeDigitada " +
                    " ${moeda.abreviacao} " + "-\n " + "${moeda.name}, " + "totalizando\n R$" + totalDaCompra
        val userEntity = UserEntity()
        //Act
        viewModel.compra(quantidadeDigitada, moeda, userEntity)

        val esperada = OperacaoCompraState.Sucesso(mensagem)
        val real = viewModel.viewOperacaoCompraState.value
        assertEquals(esperada, real)

    }

    @Test
    fun quandoVendaValida_RealizaVenda() {
        val viewModel = FragmentCambioViewModel(cambioRepository)

        val quantidadeDigitada: BigInteger = BigInteger("5")
        val moeda = Moeda(
            name = "Dollar",
            buy = BigDecimal("5"),
            sell = BigDecimal("5"),
            variation = BigDecimal("0.5"),
            abreviacao = "USD"
        )
        val totalDaVenda = quantidadeDigitada.multiply(moeda.sell.toBigInteger())
        val userEntity = UserEntity()
        val mensagem =
            "Parabéns!\n" + "Você acabou de \n vender" + " $quantidadeDigitada " +
                    " ${moeda.abreviacao} " + "-\n " + "${moeda.name}, " + "totalizando\n R$" + totalDaVenda

        //Act
        viewModel.venda(quantidadeDigitada, moeda, userEntity)

        val esperada = OperacaoVendaState.Sucesso(mensagem)
        val real = viewModel.viewOperacaoVendaState.value
        assertEquals(esperada, real)

    }
}