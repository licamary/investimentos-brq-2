package br.com.liege.investimentosbrq.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import br.com.liege.investimentosbrq.coroutines.AppContextProvider
import br.com.liege.investimentosbrq.coroutines.TestContextProvider
import br.com.liege.investimentosbrq.model.Moeda
import br.com.liege.investimentosbrq.model.RetornoRepositorioMoedasState
import br.com.liege.investimentosbrq.model.UserEntity
import br.com.liege.investimentosbrq.repository.MoedasHomeRepository
import io.mockk.*
import io.mockk.impl.annotations.MockK
import junit.framework.Assert.assertEquals
import org.junit.*
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

import java.math.BigDecimal

@RunWith(JUnit4::class)
class HomeViewModelTest {

    @get:Rule
    val instantTaskExecutor = InstantTaskExecutorRule()

    @MockK(relaxed = true)
    lateinit var moedasHomeRepository: MoedasHomeRepository

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        AppContextProvider.coroutinesContextProviderDelegate = TestContextProvider()
    }

    @Test
    fun quandoBuscaMoedasRetornaListaMoedas() {
        //AAA

        //Arrange
        val dollar = Moeda(
            name = "Dollar",
            buy = BigDecimal("5.12"),
            sell = BigDecimal("5.12"),
            variation = BigDecimal("0.5"),
            abreviacao = "USD"
        )
        val euro = Moeda(
            name = "Euro",
            buy = BigDecimal("5.12"),
            sell = BigDecimal("5.12"),
            variation = BigDecimal("0.5"),
            abreviacao = "EUR"
        )
        val bitcoin = Moeda(
            name = "Bitcoin",
            buy = BigDecimal("5.12"),
            sell = BigDecimal("5.12"),
            variation = BigDecimal("0.5"),
            abreviacao = "BTC"
        )

        val listaMoedas = listOf(dollar, euro, bitcoin)

        val viewModel = HomeViewModel(moedasHomeRepository)
        coEvery { moedasHomeRepository.buscaMoedas() } returns listaMoedas
        //Act
        viewModel.buscaMoedas()
        //Assert
        val esperada = RetornoRepositorioMoedasState.Sucesso(listaMoedas)
        val real = viewModel.viewStateRetornoMoedasRepositorio.value
        assertEquals(esperada, real)

    }

    @Test
    fun quandoVerificaUsusarioEtemUsuario() {
        //Arrange
        val viewModel = HomeViewModel(moedasHomeRepository)
        coEvery { moedasHomeRepository.buscaUsuario() } returns UserEntity()
        //act
        viewModel.verificaUsuario()
        //Assert
        coVerify { moedasHomeRepository.buscaUsuario() }
        coVerify(exactly = 0) { moedasHomeRepository.criaUsuario() }
    }

    @Test
    fun quandoVerificaUsusarioEnaoTemUsuario() {
        //Arrange
        val viewModel = HomeViewModel(moedasHomeRepository)
        coEvery { moedasHomeRepository.buscaUsuario() } returns null
        //Act
        viewModel.verificaUsuario()
        //Assert
        coVerify { moedasHomeRepository.buscaUsuario() }
        coVerify(exactly = 1) {moedasHomeRepository.criaUsuario() }
    }
}