package br.com.liege.investimentosbrq.utils
import android.util.Rational.ZERO
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import br.com.liege.investimentosbrq.coroutines.AppContextProvider
import br.com.liege.investimentosbrq.coroutines.TestContextProvider
import br.com.liege.investimentosbrq.model.Moeda
import br.com.liege.investimentosbrq.repository.MoedaWrapper
import br.com.liege.investimentosbrq.utils.moedautil.MoedaUtil.configuraAbreviacao
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.MockK
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import java.math.BigInteger.ZERO
import java.util.*
import kotlin.time.Duration.Companion.ZERO


@RunWith(JUnit4::class)
class MoedaUtilTest {

    @get:Rule
    val instantTaskExecutor = InstantTaskExecutorRule()

    @MockK

    @Before
    fun setup() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        AppContextProvider.coroutinesContextProviderDelegate = TestContextProvider()
    }

    @Test
    fun configuraAbreviacao_MoedaDollar() {
        val moedaDollar = Moeda(

            name = "Dollar",
            buy = BigDecimal.ZERO,
            sell = BigDecimal.ZERO,
            variation = BigDecimal("0.5"),
            abreviacao = "USD"
        )
       moedaDollar.configuraAbreviacao()
        //
        assertThat("USD", `is`(equalTo(moedaDollar.abreviacao)))
    }

    @Test
    fun configuraAbreviacao_MoedaEuro() {
        val moedaEuro = Moeda(

            name = "Euro",
            buy = BigDecimal.ZERO,
            sell = BigDecimal.ZERO,
            variation = BigDecimal("0.5"),
            abreviacao = "EUR"
        )
        moedaEuro.configuraAbreviacao()
        //
        assertThat("EUR", `is`(equalTo(moedaEuro.abreviacao)))
    }

    @Test
    fun configuraAbreviacao_MoedaBitcoin() {
        val moedaBitcoin = Moeda(

            name = "Bitcoin",
            buy = BigDecimal.ZERO,
            sell = BigDecimal.ZERO,
            variation = BigDecimal("0.5"),
            abreviacao = "BTC"
        )
        moedaBitcoin.configuraAbreviacao()
        //
        assertThat("BTC", `is`(equalTo(moedaBitcoin.abreviacao)))
    }

    @Test
    fun variacaoFormatada(){
        val moeda = Moeda(
            name = "Bitcoin",
            buy = BigDecimal.ZERO,
            sell = BigDecimal.ZERO,
            variation = BigDecimal("15.73"),
            abreviacao = "BTC")

            val formatacao = StringUtils().percentConverter(moeda.variation)
        assertThat(formatacao, `is`(equalTo("15,73%")))
    }

    @Test
    fun formatacaoValorDeCompraDaMoeda(){
        val moeda = Moeda(

            name = "Dollar",
            buy = BigDecimal(5.12),
            sell = BigDecimal(5.12),
            variation = BigDecimal("15.73"),
            abreviacao = "USD")

        val formatacao = StringUtils().stringConverterValorEmReal(moeda.sell.toDouble())
        assertThat(formatacao, `is`(equalTo("R$ 5,12")))
        }

    @Test
    fun formatacaoValorDeVendaDaMoeda(){
        val moeda = Moeda(
            name = "dollar",
            buy = BigDecimal(5.12),
            sell = BigDecimal(5.12),
            variation = BigDecimal("15.73"),
            abreviacao = "USD")

        val formatacao = StringUtils().stringConverterValorEmReal(moeda.buy.toDouble())
        assertThat(formatacao, `is`(equalTo("R$ 5,12")))
    }
}

