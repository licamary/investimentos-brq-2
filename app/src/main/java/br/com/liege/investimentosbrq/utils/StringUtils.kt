package br.com.liege.investimentosbrq.utils

import android.content.Context
import androidx.core.content.ContextCompat
import br.com.liege.investimentosbrq.R
import java.math.BigDecimal
import java.text.DecimalFormat

class StringUtils {

    fun percentConverter(text: BigDecimal): String {
        val decimalFormat = DecimalFormat("#.##")
        return decimalFormat.format(text) + "%"
    }

    fun stringConverterValorEmReal(saldoDisponivel: Double): String {
        val decimalFormat = DecimalFormat("#.##")
        val format = decimalFormat.format(saldoDisponivel)
        return "R$ $format".replace(".", ",")
    }

    fun setColorVariacao(variacao: BigDecimal, context: Context): Int {
        val color = if (variacao > BigDecimal.ZERO) {
            ContextCompat.getColor(context, R.color.verde)
        } else if (variacao < BigDecimal.ZERO) {
            ContextCompat.getColor(context, R.color.vermelho)
        } else ContextCompat.getColor(context, R.color.white)
        return color
    }
}



