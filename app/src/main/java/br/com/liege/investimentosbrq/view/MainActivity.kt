package br.com.liege.investimentosbrq.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.liege.investimentosbrq.viewmodel.HomeViewModel
import br.com.liege.investimentosbrq.R
import br.com.liege.investimentosbrq.model.Moeda
import br.com.liege.investimentosbrq.model.RetornoRepositorioMoedasState
import br.com.liege.investimentosbrq.utils.moedautil.MoedaUtil.configuraAbreviacao
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    lateinit var recycler: RecyclerView

    val viewModel: HomeViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        verificaUsuario()
        configuraToolBar()
        configuraRecyclerView()
        observaRetornoMoedastate()
        viewModel.buscaMoedas()
    }

    private fun verificaUsuario() {
        viewModel.verificaUsuario()
    }

    fun observaRetornoMoedastate() {
        viewModel.viewStateRetornoMoedasRepositorio.observe(this, Observer {
            when (it) {
                is RetornoRepositorioMoedasState.Sucesso -> {
                    val adapter = MoedaAdapter(this, it.listaMoedas, this::onClickMoeda)
                    Log.i("LISTA", "it.listaMoedas.get(1).name")
                    recycler.adapter = adapter
                    adapter.notifyDataSetChanged()
                }
                is RetornoRepositorioMoedasState.Erro -> {
                    Toast.makeText(this, it.mensagem, Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun configuraRecyclerView() {
        recycler = findViewById(R.id.recycler_view)
        val layoutManager = LinearLayoutManager(this)
        recycler.layoutManager = layoutManager
    }

    private fun onClickMoeda(moeda: Moeda) {
        moeda.configuraAbreviacao()
        val intent = Intent(this, ActivityCambio::class.java)
        intent.putExtra("CHAVE_MOEDA", moeda)
        startActivity(intent)
    }

    private fun configuraToolBar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar_activity_main)
        setSupportActionBar(toolbar)
        MainScope().launch {
            val titulo = findViewById<TextView>(R.id.toolbar_titulo)
            val botaoVoltar = findViewById<TextView>(R.id.toolbar_botao_voltar)
            botaoVoltar.visibility = View.INVISIBLE
            titulo.text = "Moedas"
        }
    }
}
