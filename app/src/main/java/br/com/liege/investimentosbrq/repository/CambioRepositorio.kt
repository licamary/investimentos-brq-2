package br.com.liege.investimentosbrq.repository

import br.com.liege.investimentosbrq.data.db.dao.UserDao
import br.com.liege.investimentosbrq.model.UserEntity

class CambioRepositorio(private val userDao: UserDao) {

    suspend fun buscaUsuario() = userDao.buscaUsuario(id = 1L)

    suspend fun atualizaUsuario(userEntity: UserEntity) = userDao.atualizaUsuario(userEntity)
}