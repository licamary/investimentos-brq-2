package br.com.liege.investimentosbrq.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.math.BigInteger

@Entity(tableName = "user")
data class UserEntity(

    @PrimaryKey(autoGenerate = true)
    val id: Long = 0,
    var saldodisponivel: Double = 1000000.00,
    var moedaEmCaixaDolar:  BigInteger = BigInteger.ZERO,
    var moedaEmCaixaEuro: BigInteger = BigInteger.ZERO,
    var moedaEmCaixaBitcoin: BigInteger = BigInteger.ZERO
)
