package br.com.liege.investimentosbrq.repository

import br.com.liege.investimentosbrq.data.db.dao.UserDao
import br.com.liege.investimentosbrq.model.Moeda
import br.com.liege.investimentosbrq.model.UserEntity
import br.com.liege.investimentosbrq.retrofit.service.FinanceService

class MoedasHomeRepository(private val service: FinanceService, private val userDao: UserDao) {


    suspend fun buscaMoedas(): List<Moeda> =
        MoedaWrapper.filtraMoedas(service.buscaFinance().execute().body())

    suspend fun buscaUsuario(): UserEntity? = userDao.buscaUsuario(id = 1L)

    suspend fun criaUsuario() = userDao.criaUsuario(UserEntity())

}