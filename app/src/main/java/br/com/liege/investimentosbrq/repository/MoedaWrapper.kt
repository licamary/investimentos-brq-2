package br.com.liege.investimentosbrq.repository

import br.com.liege.investimentosbrq.model.Finance
import br.com.liege.investimentosbrq.model.Moeda

object MoedaWrapper {

    fun filtraMoedas(finance: Finance?): List<Moeda> {
        val listaMoedas = ArrayList<Moeda>()
        finance?.results?.currencies?.let {
            if (it.usd.sell != null) {
                listaMoedas.add(it.usd)
            }
            if (it.jpy.sell != null) {
                listaMoedas.add(it.jpy)
            }
            if (it.gbp.sell != null) {
                listaMoedas.add(it.gbp)
            }
            if (it.eur.sell != null) {
                listaMoedas.add(it.eur)
            }
            if (it.cny.sell != null) {
                listaMoedas.add(it.cny)
            }
            if (it.cad.sell != null) {
                listaMoedas.add(it.cad)
            }
            if (it.btc.sell != null) {
                listaMoedas.add(it.btc)
            }
            if (it.aud.sell != null) {
                listaMoedas.add(it.aud)
            }
            if (it.ars.sell != null) {
                listaMoedas.add(it.ars)
            }
        }
        return listaMoedas
    }
}