package br.com.liege.investimentosbrq.view

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import br.com.liege.investimentosbrq.R
import br.com.liege.investimentosbrq.model.Moeda
import br.com.liege.investimentosbrq.model.OperacaoCompraState
import br.com.liege.investimentosbrq.model.OperacaoVendaState
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

class ActivityCambio : AppCompatActivity() {

    lateinit var moeda: Moeda

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cambio)
        buscaMoeda()
        configuraToolbar()
        inicializaFragmentCambioConfigurado()
    }

    private fun buscaMoeda() {
        if (intent.hasExtra("CHAVE_MOEDA")) {
            moeda = intent.getSerializableExtra("CHAVE_MOEDA") as Moeda
        }
    }

    private fun inicializaFragmentCambioConfigurado() {
        configuraToolbarNaCambio()
        val fragmentoCambio = CambioFragment()
        fragmentoCambio.vaiParaSucessoCompra =
            { operacaoSucesso -> iniciaFragmentSucessoCompraConfigurado(operacaoSucesso) }
        fragmentoCambio.vaiParaSucessoVenda =
            { operacaoSucesso -> iniciaFragmentSucessoVendaConfigurado(operacaoSucesso) }
        val moedaBudle = Bundle()
        moedaBudle.putSerializable("CHAVE_MOEDA", moeda)
        fragmentoCambio.arguments = moedaBudle
        iniciaFragment(fragmentoCambio)
    }

    private fun iniciaFragmentSucessoCompraConfigurado(operacaoState: OperacaoCompraState.Sucesso) {
        configuraToolbarNoSucesso("Comprar")
        val sucessoFrangment = SucessoVendaOuCompraFragment()
        val mensagemBudle = Bundle()
        mensagemBudle.putString("CHAVE_MENSAGEM", operacaoState.mensagem)
        sucessoFrangment.arguments = mensagemBudle
        iniciaFragment(sucessoFrangment)
    }

    private fun iniciaFragmentSucessoVendaConfigurado(operacaoState: OperacaoVendaState.Sucesso) {
        configuraToolbarNoSucesso("Vender")
        val sucessoFrangment = SucessoVendaOuCompraFragment()
        val mensagemBudle = Bundle()
        mensagemBudle.putString("CHAVE_MENSAGEM", operacaoState.mensagem)
        sucessoFrangment.arguments = mensagemBudle
        iniciaFragment(sucessoFrangment)
    }

    private fun configuraToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.tooblar_activity_cambio)
        setSupportActionBar(toolbar)
    }

    private fun configuraToolbarNoSucesso(mensagem: String) {
        MainScope().launch {
            val titulo = findViewById<TextView>(R.id.toolbar_titulo)
            val botaoVoltar = findViewById<TextView>(R.id.toolbar_botao_voltar)
            botaoVoltar.text = "Câmbio"
            titulo.text = mensagem
            botaoVoltar.setOnClickListener { inicializaFragmentCambioConfigurado() }
        }
    }

    private fun configuraToolbarNaCambio() {
        MainScope().launch {
            val titulo = findViewById<TextView>(R.id.toolbar_titulo)
            val botaoVoltar = findViewById<TextView>(R.id.toolbar_botao_voltar)
            botaoVoltar.text = "Moedas"
            titulo.text = "Câmbio"
            botaoVoltar.setOnClickListener { voltarParaHome() }
        }
    }

    fun voltarParaHome() {
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    fun iniciaFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainerView, fragment, null).commit()
    }
}


