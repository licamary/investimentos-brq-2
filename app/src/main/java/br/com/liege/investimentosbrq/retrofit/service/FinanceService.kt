package br.com.liege.investimentosbrq.retrofit.service

import br.com.liege.investimentosbrq.model.Finance
import retrofit2.http.GET


interface FinanceService {

    @GET ("finance?key=59535e11")
    fun buscaFinance(): retrofit2.Call<Finance>

}
