package br.com.liege.investimentosbrq.view

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import br.com.liege.investimentosbrq.R

class SucessoVendaOuCompraFragment : Fragment() {

    private lateinit var botaoHome: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_successo_venda_ou_compra, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mensagem = arguments?.getString("CHAVE_MENSAGEM")
        val campoSucesso = view.findViewById<TextView>(R.id.text_sucesso)
        campoSucesso.text = mensagem
        configuraClickParaTelaHome(view)
    }

    private fun configuraClickParaTelaHome(view: View) {
        botaoHome = view.findViewById<Button>(R.id.botao_Home)
        botaoHome.setOnClickListener() {
            Intent(requireContext(), MainActivity::class.java).apply {
                startActivity(this)
            }
        }
    }
}
