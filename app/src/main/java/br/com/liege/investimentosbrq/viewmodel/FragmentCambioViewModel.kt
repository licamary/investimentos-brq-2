package br.com.liege.investimentosbrq.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.liege.investimentosbrq.coroutines.AppContextProvider
import br.com.liege.investimentosbrq.model.*
import br.com.liege.investimentosbrq.repository.CambioRepositorio
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.math.BigInteger
import kotlin.Exception

class FragmentCambioViewModel(private val cambioRepositorio: CambioRepositorio) : ViewModel() {


    private var botaoVenderEvent = MutableLiveData<ValidaBotaoVenderEvent>()
    val viewValidaBotaoVenderEvent: LiveData<ValidaBotaoVenderEvent> = botaoVenderEvent

    private var botaoComprarEvent = MutableLiveData<ValidaBotaoComprarEvent>()
    val viewValidaBotaoComprarEvent: LiveData<ValidaBotaoComprarEvent> = botaoComprarEvent

    private var iniciaFragmentCambioState = MutableLiveData<IniciaCambioFragmentState>()
    val viewIniciaCambioFragmentState: LiveData<IniciaCambioFragmentState> =
        iniciaFragmentCambioState

    private var operacaoCompraState = MutableLiveData<OperacaoCompraState>()
    val viewOperacaoCompraState: LiveData<OperacaoCompraState> = operacaoCompraState

    private var operacaoVendaState = MutableLiveData<OperacaoVendaState>()
    val viewOperacaoVendaState: LiveData<OperacaoVendaState> = operacaoVendaState


    fun validaBotaoComprar(quantidadeDigitada: String, moeda: Moeda, userEntity: UserEntity) {

        if (!quantidadeDigitada.isNullOrBlank()) {

            val valorDaCompra = quantidadeDigitada.toBigDecimal() * moeda.buy

            val validaSaldoDisponivel = userEntity.saldodisponivel.toBigDecimal() >= valorDaCompra

            val validaQuantidadeMaiorZero = quantidadeDigitada.toBigInteger() > BigInteger.ZERO

            val ehValido: Boolean = validaSaldoDisponivel && validaQuantidadeMaiorZero

            if (ehValido) {
                botaoComprarEvent.postValue(ValidaBotaoComprarEvent.Ativado)
            } else {
                botaoComprarEvent.postValue(ValidaBotaoComprarEvent.Desativado)
            }
        } else botaoComprarEvent.postValue(ValidaBotaoComprarEvent.Desativado)
    }

    fun validaBotaoVender(quantidadeDigitada: String, moeda: Moeda, userEntity: UserEntity) {

        val moedaEmCaixa = when (moeda.name) {
            "Dollar" -> userEntity.moedaEmCaixaDolar
            "Bitcoin" -> userEntity.moedaEmCaixaBitcoin
            "Euro" -> userEntity.moedaEmCaixaEuro
            else -> throw Exception("Moeda nao encontrada")
        }

        if (!quantidadeDigitada.isNullOrBlank()) {

            quantidadeDigitada.toBigDecimal() * moeda.sell

            val validaMoedaDisponivel =
                moedaEmCaixa.toBigDecimal() >= quantidadeDigitada.toBigDecimal()

            val validaQuantidadeMaiorZero = quantidadeDigitada.toBigInteger() > BigInteger.ZERO

            val ehValido: Boolean = validaMoedaDisponivel && validaQuantidadeMaiorZero

            if (ehValido) {
                botaoVenderEvent.postValue(ValidaBotaoVenderEvent.Ativado)
            } else {
                botaoVenderEvent.postValue(ValidaBotaoVenderEvent.Desativado)
            }
        } else botaoVenderEvent.postValue(ValidaBotaoVenderEvent.Desativado)
    }

    fun iniciaCambioFragment(moeda: Moeda) {
        CoroutineScope(AppContextProvider.io).launch {
            try {
                val usuario = cambioRepositorio.buscaUsuario()
                val saldodisponivel = usuario.saldodisponivel
                var moedaEmCaixa = filtraMoedaParaCarteira(moeda, usuario)
                iniciaFragmentCambioState.postValue(
                    moedaEmCaixa?.let {
                        IniciaCambioFragmentState.Sucesso(
                            saldodisponivel,
                            it, usuario
                        )
                    }
                )
            } catch (error: Exception) {
                iniciaFragmentCambioState.postValue(
                    IniciaCambioFragmentState.Erro(error.message.toString())
                )
            }
        }
    }

    private fun filtraMoedaParaCarteira(
        moeda: Moeda,
        usuario: UserEntity
    ): BigInteger? {
        var moedaEmCaixa = when (moeda.name) {
            "Dollar" -> usuario.moedaEmCaixaDolar
            "Euro" -> usuario.moedaEmCaixaEuro
            "Bitcoin" -> usuario.moedaEmCaixaBitcoin
            else -> {
                BigInteger.ZERO
            }
        }
        return moedaEmCaixa
    }

    fun compra(quantidade: BigInteger, moeda: Moeda, usuario: UserEntity) {
        try {
            atualizaSaldoAposCompra(quantidade, moeda, usuario)
            atualizaMoedaEmCaixaAposCompra(quantidade, moeda, usuario)
            atualizarUsuarioNoBancoDeDados(usuario)
            val totalDaCompra = quantidade.toBigDecimal() * moeda.buy
            val mensagem =
                "Parabéns!\n" + "Você acabou de \n comprar" + " $quantidade  ${moeda.abreviacao} " + "-\n " + "${moeda.name}, " + "totalizando\n R$" + totalDaCompra
            operacaoCompraState.postValue(
                OperacaoCompraState.Sucesso(mensagem)

            )
        } catch (erro: Exception) {
            operacaoCompraState.postValue(
                OperacaoCompraState.Erro("Compra invalida")
            )
        }
    }

    fun venda(quantidade: BigInteger, moeda: Moeda, usuario: UserEntity) {
        try {
            atualizaSaldoAposVenda(quantidade, moeda, usuario)
            atualizaMoedaAposVenda(quantidade, moeda, usuario)
            atualizarUsuarioNoBancoDeDados(usuario)
            val totalDaVenda = quantidade.toBigDecimal() * moeda.sell
            val mensagem =
                "Parabéns!\n" + "Você acabou de \n vender" + " $quantidade  ${moeda.abreviacao} " + "-\n " + "${moeda.name}, " + "totalizando\n R$" + totalDaVenda

            operacaoVendaState.postValue(
                OperacaoVendaState.Sucesso(mensagem)
            )
        } catch (erro: Exception) {
            operacaoVendaState.postValue(
                OperacaoVendaState.Erro("Venda invalida")
            )
        }
    }

    private fun atualizarUsuarioNoBancoDeDados(userEntity: UserEntity) {
        CoroutineScope(AppContextProvider.io).launch {
            cambioRepositorio.atualizaUsuario(userEntity)
        }
    }

    fun atualizaSaldoAposCompra(quantidade: BigInteger, moeda: Moeda, userEntity: UserEntity) {
        val valorDaCompra = quantidade.toDouble() * moeda.buy.toDouble()
        userEntity.saldodisponivel -= valorDaCompra
    }

    fun atualizaMoedaEmCaixaAposCompra(
        quantidade: BigInteger,
        moeda: Moeda,
        userEntity: UserEntity
    ) {
        when (moeda.name) {
            "Dollar" -> userEntity.moedaEmCaixaDolar += quantidade
            "Euro" -> userEntity.moedaEmCaixaEuro += quantidade
            "Bitcoin" -> userEntity.moedaEmCaixaBitcoin += quantidade
            else -> throw Exception("Moeda nao encontrada")
        }
    }

    fun atualizaSaldoAposVenda(quantidade: BigInteger, moeda: Moeda, userEntity: UserEntity) {
        val valorDaVenda = quantidade.toDouble() * moeda.sell.toDouble()
        userEntity.saldodisponivel += valorDaVenda
    }

    fun atualizaMoedaAposVenda(
        quantidade: BigInteger,
        moeda: Moeda,
        userEntity: UserEntity
    ) {
        when (moeda.name) {
            "Dollar" -> userEntity.moedaEmCaixaDolar -= quantidade
            "Euro" -> userEntity.moedaEmCaixaEuro -= quantidade
            "Bitcoin" -> userEntity.moedaEmCaixaBitcoin -= quantidade
            else -> throw Exception("Moeda nao encontrada")
        }
    }
}