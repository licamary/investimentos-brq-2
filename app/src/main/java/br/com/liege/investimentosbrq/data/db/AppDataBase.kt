package br.com.liege.investimentosbrq.data.db
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import br.com.liege.investimentosbrq.data.db.dao.UserDao
import br.com.liege.investimentosbrq.data.db.typeConverter.BigIntegerConverter
import br.com.liege.investimentosbrq.model.UserEntity

@TypeConverters(BigIntegerConverter::class)
@Database(entities = [UserEntity::class], version = 2, exportSchema = false)
abstract class AppDataBase : RoomDatabase() {
    abstract  val userDao : UserDao
}