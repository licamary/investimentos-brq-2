package br.com.liege.investimentosbrq.di

import androidx.room.Room
import br.com.liege.investimentosbrq.viewmodel.FragmentCambioViewModel
import br.com.liege.investimentosbrq.viewmodel.HomeViewModel
import br.com.liege.investimentosbrq.data.db.AppDataBase
import br.com.liege.investimentosbrq.data.db.dao.UserDao
import br.com.liege.investimentosbrq.repository.CambioRepositorio
import br.com.liege.investimentosbrq.repository.MoedasHomeRepository
import br.com.liege.investimentosbrq.retrofit.MoedasRetrofit
import br.com.liege.investimentosbrq.retrofit.service.FinanceService
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    single<AppDataBase> { Room.databaseBuilder(get(), AppDataBase::class.java, "appdatabase").fallbackToDestructiveMigration().build() }
    single <UserDao> { get<AppDataBase>().userDao }
    single<CambioRepositorio> { CambioRepositorio(get<UserDao>())}

    single <FinanceService> { MoedasRetrofit().retornaFinance()}
    single <MoedasHomeRepository>{ MoedasHomeRepository(get<FinanceService>(),get())}

    viewModel<FragmentCambioViewModel> { FragmentCambioViewModel(get<CambioRepositorio>())}
    viewModel<HomeViewModel> { HomeViewModel(get<MoedasHomeRepository>()) }

}
