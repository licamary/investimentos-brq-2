package br.com.liege.investimentosbrq.utils.moedautil

import br.com.liege.investimentosbrq.model.Moeda

object MoedaUtil {
    fun Moeda.configuraAbreviacao() {

        when (name) {
            "Dollar" -> {
                abreviacao = "USD"
            }
            "Euro" -> {
                abreviacao = "EUR"
            }
            "Bitcoin" -> {
              abreviacao = "BTC"

            }
        }
    }
}
