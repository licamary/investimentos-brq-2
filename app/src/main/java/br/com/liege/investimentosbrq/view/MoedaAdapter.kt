package br.com.liege.investimentosbrq.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import br.com.liege.investimentosbrq.R
import br.com.liege.investimentosbrq.model.Moeda
import br.com.liege.investimentosbrq.utils.StringUtils
import java.math.BigDecimal

class MoedaAdapter(
    val context: Context, val listaDeMoedas: List<Moeda>, val onClickMoeda: (moeda: Moeda) -> Unit

) : RecyclerView.Adapter<MoedaAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.activity_card_moedas, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val moeda = listaDeMoedas[position]
        holder.itemView.setOnClickListener { onClickMoeda.invoke(moeda) }
        holder.vincula(moeda)
    }

    override fun getItemCount(): Int {
        return listaDeMoedas.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private lateinit var moeda: Moeda
        private val simboloMoeda: TextView = view.findViewById(R.id.textViewMoeda)
        private val simboloVariacao: TextView = view.findViewById(R.id.variacao)

        fun vincula(moeda: Moeda) {
            this.moeda = moeda
            simboloMoeda.text = moeda.name
            setCampoVariacao(moeda)
        }

        private fun setCampoVariacao(moeda: Moeda) {
            simboloVariacao.text =
                formatoMoedaPorcentagem(moeda.variation)
            val corVariacao = StringUtils().setColorVariacao(moeda.variation, context)
            simboloVariacao.setTextColor(corVariacao)
        }

        private fun formatoMoedaPorcentagem(big: BigDecimal): String {
            val valorFormatado = big.setScale(2, BigDecimal.ROUND_HALF_EVEN)
            return "$valorFormatado%"
        }
    }
}




