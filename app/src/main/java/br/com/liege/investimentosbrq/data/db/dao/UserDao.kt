package br.com.liege.investimentosbrq.data.db.dao

import androidx.room.*
import br.com.liege.investimentosbrq.model.UserEntity

@Dao
abstract class UserDao {

    @Insert
    abstract fun criaUsuario(user: UserEntity)

    @Update
    abstract fun atualizaUsuario(user: UserEntity)

    @Query("SELECT * FROM user WHERE id == :id")
    abstract fun buscaUsuario(id: Long ): UserEntity

}