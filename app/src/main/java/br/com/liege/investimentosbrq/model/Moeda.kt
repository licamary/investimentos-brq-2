package br.com.liege.investimentosbrq.model

import java.io.Serializable
import java.math.BigDecimal

data class Moeda(

    val name: String,
    val buy: BigDecimal,
    val sell: BigDecimal,
    val variation: BigDecimal,
    var abreviacao: String,
) : Serializable




