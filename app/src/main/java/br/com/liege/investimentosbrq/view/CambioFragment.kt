package br.com.liege.investimentosbrq.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import br.com.liege.investimentosbrq.R
import br.com.liege.investimentosbrq.viewmodel.FragmentCambioViewModel
import br.com.liege.investimentosbrq.model.*
import br.com.liege.investimentosbrq.utils.StringUtils
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.math.BigInteger

class CambioFragment : Fragment() {

    private lateinit var textMoedaEmCaixa: TextView
    private lateinit var textSaldoDisponivel: TextView
    private lateinit var editQuantidade: EditText
    private lateinit var botaoComprar: Button
    private lateinit var botaoVender: Button
    lateinit var viewCambio: View
    lateinit var moeda: Moeda
    lateinit var userEntity: UserEntity
    val viewModel: FragmentCambioViewModel by viewModel()
    var vaiParaSucessoCompra: ((operacaoCompraSucesso: OperacaoCompraState.Sucesso) -> Unit)? = null
    var vaiParaSucessoVenda: ((operacaoVendaSucesso: OperacaoVendaState.Sucesso) -> Unit)? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_cambio, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewCambio = view
        initCambioFragment()
        initViews()
        setObservers()
        observadorDoCampoQuantidade()
    }

    private fun initCambioFragment() {
        moeda = arguments?.get("CHAVE_MOEDA") as Moeda
        viewModel.iniciaCambioFragment(moeda)
    }

    private fun setObservers() {
        observerValidacaoBotaoComprarEvent()
        observerValidacaoBotaoVenderEvent()
        observerInicioDoFragment()
        observaOperacaoCompra()
        observaOperacaoVenda()
    }

    private fun observaOperacaoVenda() {
        viewModel.viewOperacaoVendaState.observe(
            viewLifecycleOwner, Observer {
                when (it) {
                    is OperacaoVendaState.Sucesso -> vaiParaSucessoVenda?.invoke(it)
                    is OperacaoVendaState.Erro -> {
                    }
                }
            }
        )
    }

    private fun observaOperacaoCompra() {
        viewModel.viewOperacaoCompraState.observe(
            viewLifecycleOwner, Observer {
                when (it) {
                    is OperacaoCompraState.Sucesso -> vaiParaSucessoCompra?.invoke(it)
                    is OperacaoCompraState.Erro -> {
                    }
                }
            }
        )
    }

    private fun observerInicioDoFragment() {
        viewModel.viewIniciaCambioFragmentState.observe(
            viewLifecycleOwner, Observer {
                when (it) {
                    is IniciaCambioFragmentState.Sucesso -> {
                        userEntity = it.userEntity
                        setCamposTelaCambio(it.saldoDisponivel, it.moedaEmCaixa)
                    }
                    is IniciaCambioFragmentState.Erro -> {
                    }
                }
            }
        )
    }

    private fun observerValidacaoBotaoVenderEvent() {
        viewModel.viewValidaBotaoVenderEvent.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    is ValidaBotaoVenderEvent.Ativado -> setBotaoVenderAtivado()
                    is ValidaBotaoVenderEvent.Desativado -> setBotaoVenderDesativado()
                }
            }
        )
    }

    private fun setBotaoVenderDesativado() {
        botaoVender.setBackgroundResource(R.drawable.buttom_shape_desativado)
        botaoVender.isClickable = false
        botaoVender.setOnClickListener() {}
    }

    private fun setBotaoVenderAtivado() {
        botaoVender.setBackgroundResource(R.drawable.buttom_shape_ativado)
        botaoVender.isClickable = true
        botaoVender.setOnClickListener() {
            val quantidade = editQuantidade.text.toString().toBigInteger()
            viewModel.venda(quantidade, moeda, userEntity)
        }
    }

    private fun observerValidacaoBotaoComprarEvent() {
        viewModel.viewValidaBotaoComprarEvent.observe(
            viewLifecycleOwner,
            Observer {
                when (it) {
                    is ValidaBotaoComprarEvent.Ativado -> setBotaoComprarAtivado()
                    is ValidaBotaoComprarEvent.Desativado -> setBotaoComprarDesativado()
                }
            }
        )
    }

    private fun setBotaoComprarDesativado() {
        botaoComprar.setBackgroundResource(R.drawable.buttom_shape_desativado)
        botaoComprar.isClickable = false
        botaoComprar.setOnClickListener() {}
    }

    private fun setBotaoComprarAtivado() {
        botaoComprar.setBackgroundResource(R.drawable.buttom_shape_ativado)
        botaoComprar.isClickable = true
        botaoComprar.setOnClickListener() {
            val quantidade = editQuantidade.text.toString().toBigInteger()
            viewModel.compra(quantidade, moeda, userEntity)
        }
    }

    private fun observadorDoCampoQuantidade() {
        setBotaoVenderDesativado()
        setBotaoComprarDesativado()
        editQuantidade.doAfterTextChanged { text ->
            viewModel.validaBotaoComprar(text.toString(), moeda, userEntity)
            viewModel.validaBotaoVender(text.toString(), moeda, userEntity)
        }
    }

    private fun setCamposTelaCambio(saldoDisponivel: Double, moedaEmCaixa: BigInteger) {

        val textViewAbreviacao = viewCambio.findViewById<TextView>(R.id.textview_moeda_tela_cambio)
        val abreviacaoFormatada = "${moeda.abreviacao} - ${moeda.name}"
        textViewAbreviacao.text = abreviacaoFormatada

        val textViewVariacao = viewCambio.findViewById<TextView>(R.id.variacao_tela_cambio)
        val corVariacao = StringUtils().setColorVariacao(moeda.variation, requireContext())
        textViewVariacao.setTextColor(corVariacao)
        val variacaoFormatada = StringUtils().percentConverter(moeda.variation)
        textViewVariacao.text = variacaoFormatada

        val textViewCompra = viewCambio.findViewById<TextView>(R.id.compra_tela_cambio)
        val campoCompraFormatada =
            "Compra: ${StringUtils().stringConverterValorEmReal(moeda.buy.toDouble())}"
        textViewCompra.text = campoCompraFormatada

        val textViewVenda = viewCambio.findViewById<TextView>(R.id.venda_tela_cambio)
        val campoVendaFormatada =
            "Venda: ${StringUtils().stringConverterValorEmReal(moeda.sell.toDouble())}"
        textViewVenda.text = campoVendaFormatada

        val campoMoedaEmCaixaFortamado = "${moedaEmCaixa} ${moeda.name} em caixa"
        textMoedaEmCaixa.text = campoMoedaEmCaixaFortamado

        val campoSaldoDisponivelFormatado =
            "Saldo disponivel: " + StringUtils().stringConverterValorEmReal(saldoDisponivel)
        textSaldoDisponivel.text = campoSaldoDisponivelFormatado
    }

    private fun initViews() {
        textMoedaEmCaixa = viewCambio.findViewById<TextView>(R.id.moeda_em_caixa)
        textSaldoDisponivel = viewCambio.findViewById<TextView>(R.id.textview_saldo_disponivel)
        botaoVender = viewCambio.findViewById<Button>(R.id.button_vender)
        botaoComprar = viewCambio.findViewById<Button>(R.id.button_comprar)
        editQuantidade = viewCambio.findViewById<EditText>(R.id.edit_quantidade)
    }
}


