package br.com.liege.investimentosbrq.retrofit

import br.com.liege.investimentosbrq.retrofit.service.FinanceService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MoedasRetrofit {

    private val retrofit = retrofitBase()
    private val serviceFinance: FinanceService = retrofit.create(FinanceService::class.java)
    private fun retrofitBase() = Retrofit.Builder()
        .baseUrl("https://api.hgbrasil.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun retornaFinance(): FinanceService{
        return retrofitBase().create(FinanceService::class.java)
    }
}



