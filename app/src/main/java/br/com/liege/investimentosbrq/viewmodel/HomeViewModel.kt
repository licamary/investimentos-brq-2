package br.com.liege.investimentosbrq.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.liege.investimentosbrq.coroutines.AppContextProvider
import br.com.liege.investimentosbrq.model.RetornoRepositorioMoedasState
import br.com.liege.investimentosbrq.repository.MoedasHomeRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class HomeViewModel (private val moedasHomeRepository: MoedasHomeRepository ) : ViewModel(){


    private var stateRetornoMoedasRepositorio = MutableLiveData<RetornoRepositorioMoedasState>()
    val viewStateRetornoMoedasRepositorio: LiveData<RetornoRepositorioMoedasState> =
        stateRetornoMoedasRepositorio

    fun buscaMoedas() {
        CoroutineScope(AppContextProvider.io).launch {
            try {
                val listaMoedas = moedasHomeRepository.buscaMoedas()

                stateRetornoMoedasRepositorio.postValue(
                    RetornoRepositorioMoedasState.Sucesso(
                        listaMoedas
                    )
                )
            } catch (error: Throwable) {
                stateRetornoMoedasRepositorio.postValue(
                    RetornoRepositorioMoedasState.Erro(error.message ?: "Ocorreu um erro")
                )
            }
        }
    }

    fun verificaUsuario() {
        CoroutineScope(AppContextProvider.io).launch {
            val usuario = moedasHomeRepository.buscaUsuario()
            if(usuario == null){
               moedasHomeRepository.criaUsuario()
            }
        }
    }
}

