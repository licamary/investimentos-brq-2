package br.com.liege.investimentosbrq.model

import java.math.BigInteger

sealed class RetornoRepositorioMoedasState {
    data class Sucesso(val listaMoedas: List<Moeda>) : RetornoRepositorioMoedasState()
    data class Erro(val mensagem: String) : RetornoRepositorioMoedasState()
}

sealed class IniciaCambioFragmentState {
    data class Sucesso(
        val saldoDisponivel: Double,
        val moedaEmCaixa: BigInteger,
        val userEntity: UserEntity
    ) : IniciaCambioFragmentState()

    data class Erro(val mensagem: String) : IniciaCambioFragmentState()
}

sealed class ValidaBotaoComprarEvent {
    object Ativado : ValidaBotaoComprarEvent()
    object Desativado : ValidaBotaoComprarEvent()
}

sealed class ValidaBotaoVenderEvent {
    object Ativado : ValidaBotaoVenderEvent()
    object Desativado : ValidaBotaoVenderEvent()
}

sealed class OperacaoCompraState {

    data class Sucesso (val mensagem: String):OperacaoCompraState()
    data class Erro(val mensagem: String) : OperacaoCompraState()
}

sealed class OperacaoVendaState {

    data class Sucesso (val mensagem: String):OperacaoVendaState()
    data class Erro(val mensagem: String) : OperacaoVendaState()
}

